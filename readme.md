# geomap-api

![logo.png](logo.png)

#### description

A simple `crystal-lang` app running on `heroku` providing `geo2ip` & geomap data

#### heroku

Open <https://geomap-api.herokuapp.com> in your browser

#### usage

    # crystal geomap-api.cr

    # DEBUG=true crystal geomap-api.cr

#### endpoints

- `/api/geomap/json`
- `/api/geomap/png`
- `/api/geomap/html`

#### query params

- `ip`
- `zoom`
- `marker`

#### depends on

- `ifconfig.co`
- `static-maps.yandex.ru`
- `openstreetmap.org`
