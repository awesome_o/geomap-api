require "http/server"
require "json"

debug = ENV["DEBUG"] ||= "false"

bind  = ENV["BIND"]  ||= "0.0.0.0"
port  = ENV["PORT"]  ||= "8080"
port  = port.to_i

git = "https://gitlab.com/awesome_o/geomap-api"

routes                     = {} of String => String
routes["/api/geomap/json"] = "Respond w/ JSON"
routes["/api/geomap/png"]  = "Redirect to PNG  GeoMap"
routes["/api/geomap/html"] = "Redirect to HTML GeoMap"
routes["/api/geomap/"]     = "Redirect to JSON API endpoint"
routes["/"]                = "Redirect to JSON API endpoint"
routes["/git"]             = "Redirect to GIT"
routes["/mp4"]             = "Random MP4 video"

debug == "true" && puts routes.to_pretty_json

server = HTTP::Server.new do |c|

    ## process request
    req = c.request
    headers = req.headers
    path    = req.path
    params  = req.query_params.to_h
    method  = req.method

    # conditional request body handling
    if method == "POST"
        if headers["Content-Type"] == "application/json"
            body = JSON.parse( req.body.as(IO).gets_to_end.to_s )
        else
            body = req.body.as(IO).gets_to_end.to_s
        end
    else
        body = nil
    end

    request =                        \
    {                                \
        "http" =>                    \
        {                            \
            "headers" => headers   , \
            "method"  => method    , \
            "path"    => path      , \
            "params"  => params    , \
            "body"    => body        \
        }                            \
    }

    ## debug output
    debug == "true" && puts request.to_pretty_json

    ## ffu get language from headers and / or query_params
    ## puts headers["Accept-Language"]

    ## get geolookup ip either from x-forwarded-for header or from url query params
    if headers.has_key?("X-Forwarded-For") ; ip = headers["X-Forwarded-For"] ; end
    if params.has_key?("ip")               ; ip = params["ip"]               ; end

    ## run geoLookup
    if ip.nil?
        geo = HTTP::Client.get "https://ifconfig.co/json"
    else
        ## dirty handling of ip query param = *
        ## FFU handle ipv6 gracefully through URL encode
        if ip == "*"
            geo = HTTP::Client.get "https://ifconfig.co/json?ip=8.8.8.8"
        else
            geo = HTTP::Client.get "https://ifconfig.co/json?ip=#{ip}"
        end
    end
    if ! geo.nil?
        geo_j = JSON.parse( geo.body )
        geo_h = geo_j.as_h
        if geo_h.has_key?("latitude")  ; latitude  = geo_h["latitude"]  ; end
        if geo_h.has_key?("longitude") ; longitude = geo_h["longitude"] ; end
    end

    ## check query params
    if params.has_key?("ip")       ; ip        = params["ip"]       ; end
    if params.has_key?("zoom")     ; zoom      = params["zoom"]     ; end
    if params.has_key?("marker")   ; marker    = params["marker"]   ; end
    if zoom.nil?                   ; zoom      = 10                 ; end
    if marker.nil?                 ; marker    = "round"            ; end

    ## enrich w/ map data if geodata available
    if ! latitude.nil? && ! longitude.nil?
      png  = "https://static-maps.yandex.ru/1.x/?lang=en_US&size=450,450&z=#{zoom}&l=map&ll=#{longitude},#{latitude}&pt=#{longitude},#{latitude},#{marker}"
      html = "https://www.openstreetmap.org/export/embed.html?marker=#{latitude}%2C#{longitude}"
    end

    ## process response

    ## build response object
    response =                       \
    {                                \
        "http" =>                    \
        {                            \
            "headers" => headers   , \
            "method"  => method    , \
            "path"    => path      , \
            "params"  => params      \
        }                          , \
        "geo" => geo_h             , \
        "map" =>                     \
        {                            \
              "png"  => png        , \
              "html" => html         \
        }                          , \
        "git" => git                 \
    }
    resp = c.response
    debug == "true" && puts response.to_pretty_json

    ## conditional response routing
    if routes.has_key?(path.to_s)
        if path.to_s == "/api/geomap/png"
            resp.headers["Location"] = "#{png}"
            resp.status_code = 302
            resp.close
        end
        if path.to_s == "/api/geomap/html"
            resp.headers["Location"] = "#{html}"
            resp.status_code = 302
            resp.close
        end
        if path.to_s == "/api/geomap/json"
            resp.content_type = "application/json"
            resp.print response.to_pretty_json
        end
        if path.to_s == "/api/geomap/"
            resp.headers["Location"] = "./json"
            resp.status_code = 302
            resp.close
        end
        if path.to_s == "/"
            resp.headers["Location"] = "./api/geomap/json"
            resp.status_code = 302
            resp.close
        end
        if path.to_s == "/git"
            resp.headers["Location"] = git
            resp.status_code = 302
            resp.close
        end
        if path.to_s == "/mp4"
            dirname = "./mp4"
            file = Dir.open(dirname).children.shuffle[0]
            debug == "true" && puts "#{dirname}/#{file}"
            filename = "#{dirname}/#{file}"
            resp.content_type = "video/mp4"
            resp.content_length = File.size(filename)
            File.open(filename) do |file|
                IO.copy(file, resp)
            end
            resp.close
        end
    else
        resp.status_code = 404
        ## 404 response w/ image/png content
        dirname = "./404"
        file = Dir.open(dirname).children.shuffle[0]
        debug == "true" && puts "#{dirname}/#{file}"
        filename = "#{dirname}/#{file}"
        resp.content_type = "image/png"
        resp.content_length = File.size(filename)
        File.open(filename) do |file|
            IO.copy(file, resp)
        end
        resp.close
    end

end

address = server.bind_tcp bind, port
server.listen
